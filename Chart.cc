/* 
 * Pawel Kubik
 * Wykres slupkowy
 */

#include "Chart.h"
#include <iostream>
#include <iomanip>

Chart::Chart(Score& document, string& k) : Board(document, k) {
	max = 0;
	shape = '>';
	lenght = 50;
}

void Chart::draw() const {
	const list<Record>& rec = doc.getRecords();
	prepare();
	unsigned int counter = 1;
	cout<<"#  "<<left;
	cout.width(16);
	cout<<"Name"<<" |0";
	cout.width(lenght-2);
	cout<<right<<max<<'|'<<"\n";
	for(list<Record>::const_iterator it=rec.begin();it!=rec.end();++it) {
		cout<<counter;
		cout<<". ";
		cout<<left;
		cout.width(16);
		cout<<it->name.substr(0,16)<<" |"<<setfill(shape);
		cout.width(it->points*lenght/max);
		cout<<right<<' '<<setfill(' ')<<"\n";
		counter++;
	}
}

void Chart::update() {
	max = 0;	//skalowanie wykresu
	const list<Record>& rec = doc.getRecords();
	for(list<Record>::const_iterator it=rec.begin();it!=rec.end();++it) {
		if(it->points > max) 
			max = it->points;
	}
}

void Chart::setShape(char sh) {
	shape = sh;
}

void Chart::processActions() {
	Board::processKey();
	processKey();
}

void Chart::processKey() {
	if(!key.compare("s")) {
		cout<<"Enter new shape character: ";
		char c;
		cin>>c;
		setShape(c);
	}
}