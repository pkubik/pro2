CC=g++
CFLAGS=-g -Wall
LFLAGS=-g
EXECUTABLE=scoreboard
SOURCES=$(wildcard *.cc *.c)
OBJS=$(SOURCES:.cc=.o)

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	@echo "Budowanie $(EXECUTABLE)"
	$(CC) $(OBJS) $(LFLAGS) -o $(EXECUTABLE)

.cc.o:
	@echo "Budowanie $@"
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f *.o
	

