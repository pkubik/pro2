/* 
 * Pawel Kubik
 * Wykres slupkowy
 */

#include "Board.h"

#ifndef CHART_H
#define	CHART_H

class Chart : public Board {
private:
	int max;
	char shape;
	int lenght;
public:
	Chart(Score& document, string& k);
	virtual void draw() const;
	virtual void update();
	virtual void processActions();
	virtual void processKey();
	void setShape(char sh);
};

#endif	/* CHART_H */

