/* 
 * Pawel Kubik
 * Klasa ogolna tablicy wynikow
 */

#include "View.h"
#include "Score.h"

#ifndef BOARD_H
#define	BOARD_H

class Board : public View {
protected:
	Score& doc;
	string& key;
public:
	Board(Score& document, string& k);
	void prepare() const;
	void clear() const;
	virtual void processKey();
	void help() const;
};

#endif	/* BOARD_H */

