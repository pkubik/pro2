/* 
 * Pawel Kubik
 * Abstrakcyjny dokument
 */

#include <list>

using namespace std;

#ifndef DOCUMENT_H
#define	DOCUMENT_H
class View;

class Document {
private:
	list<View*> views;	//lista nie zmienia kolejnosci przy usuwaniu
	list<View*>::iterator active_element;
protected:
	View* activeView() const { return *active_element; }
public:
	Document();
	void draw() const;
	void addView(View& view);
	void nextView();
	void passControl() const;
	
};

#endif	/* DOCUMENT_H */

