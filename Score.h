/* 
 * Pawel Kubik
 * Wyniki (dokument)
 */

#include "Document.h"
#include <string>
#include <list>

using namespace std;

#ifndef SCORE_H
#define	SCORE_H

struct Record {
	string name;
	int points;
	Record(const string& nam, int pts);
};

class Score : public Document {
private:
	struct {
		bool operator() (const Record& r1, const Record& r2) {
			return r1.name < r2.name;
		}
	} byNameCmp;
	
	struct {
		bool operator() (const Record& r1, const Record& r2) {
			return r1.points < r2.points;
		}
	} byPointsCmp;
	
	list<Record> records;
public:
	void addRecord(const string& name, int points);
	const list<Record>& getRecords() const { return records; }
	void removeRecord(int pos);
	void sortByName();
	void sortByPoints();
	void clear();
};

#endif	/* SCORE_H */

