/* 
 * Pawel Kubik
 * Klasa widoku
 */

#ifndef VIEW_H
#define	VIEW_H

class View {
public:
	virtual void draw() const = 0;
	virtual void update() = 0;
	virtual void processActions() = 0;
};

#endif	/* VIEW_H */

