/* 
 * Pawel Kubik
 * Abstrakcyjny dokument
 */

#include "Document.h"
#include "View.h"

Document::Document()
{
	active_element = views.end();
}

void Document::draw() const {
	(*active_element)->draw();
}

void Document::addView(View& view) {
	views.push_back(&view);
	if(active_element == views.end())
		active_element = views.begin();
}

void Document::nextView() {
	++active_element;
	if(active_element == views.end()) {
		active_element = views.begin();
	}
	(*active_element)->update();
}

void Document::passControl() const {
	(*active_element)->processActions();
}