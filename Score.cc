/* 
 * Pawel Kubik
 * Wyniki (dokument)
 */

#include "Score.h"

Record::Record(const string& nam, int pts) : name(nam), points(pts) {}

void Score::addRecord(const string& name, int points)
{
	records.push_back(Record(name, points));
}

void Score::clear() {
	records.clear();
}

void Score::removeRecord(int pos) {
	list<Record>::iterator it = records.begin();
	advance(it,pos-1);
	records.erase(it);
}

void Score::sortByName() {
	records.sort(byNameCmp);
}

void Score::sortByPoints() {
	records.sort(byPointsCmp);
}