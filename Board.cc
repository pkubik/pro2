/* 
 * Pawel Kubik
 * Klasa ogolna tablicy wynikow
 */

#include "Board.h"
#include "readkey.h"
#include <iostream>
#include <cstdlib>
#include <iomanip>

using namespace std;

Board::Board(Score& document, string& k) : doc(document), key(k) {
}

void Board::prepare() const {
	system("clear");
	cout<<"----------===============/||| Scoreboard |||\\===============-----------\n";
	cout.width(71);
	cout<<right<<"F1 - help\n";
	for(int i=0;i<71;++i)
		wcout<<L"\xE2\x94\x81";
	cout<<"\n";
}

void Board::clear() const {
	doc.clear();
}

void Board::help() const {
	system("clear");
	cout<<"Following keybindings are available:\n";
	cout<<"C - clear table\n";
	cout<<"ESC - quit\n";
	cout<<"TAB - switch view\n";
	cout<<"\n--==##Table View:\n";
	cout<<"UP ARROW - move up\n";
	cout<<"DOWN ARROW - move down\n";
	cout<<"n and p - sort by name or points respectively\n";
	cout<<"s - select record by giving number\n";
	cout<<"a - add new record\n";
	cout<<"r - remove selected record\n";
	cout<<"S or L - save to file and load from file respectively\n";
	cout<<"\n--==##Chart View:\n";
	cout<<"s - customize chart appearance\n";
	cout<<"Press any key to continue.\n";
	awaitKey();
}

void Board::processKey() {
	if(key == "C") {
		clear();
		update();
	}
	else if(key == "\x1bOP") {
		help();
	}
	else if(key == "\t") {
		doc.nextView();
	}
}