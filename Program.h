/* 
 * Pawel Kubik
 * Klasa glowna programu
 */

#include "Score.h"
#include "Table.h"
#include "Chart.h"

#ifndef PROGRAM_H
#define	PROGRAM_H

class Program {
private:
	Score doc;
	Table tab;
	Chart chart;
	string key;
	bool quitFlag;
public:
	Program();
	void passControl();
	bool getQuitFlag() const { return quitFlag; }
};

#endif	/* PROGRAM_H */

