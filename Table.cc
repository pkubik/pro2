/* 
 * Pawel Kubik
 * Implementacja widoku tabeli
 */

#include "Table.h"
#include "readkey.h"
#include <iostream>
#include <fstream>

Table::Table(Score& document, string& k) : Board(document, k) {
	selection = 1;
}

void Table::draw() const {
	const list<Record>& rec = doc.getRecords();
	unsigned int counter = 1;
	prepare();
	cout<<"#  "<<left;
	cout.width(52);
	cout<<"Name"<<" : "<<"Score\n";
	for(list<Record>::const_iterator it=rec.begin();it!=rec.end();++it) {
		cout<<counter;
		if(counter == selection) {
			cout<<".";wcout<<L"\xE2\x96\xBA";
		}
		else
			cout<<". ";
		cout<<left;
		cout.width(52);
		cout<<it->name.substr(0,40)<<" : "<<it->points<<"\n";
		counter++;
	}
}

void Table::update() {
	unsigned int l = doc.getRecords().size();
	if(selection > l) {
		selection = l;
	}
}

void Table::select() {
	cout<<"Enter the line number: ";
	cin>>selection;
	if(selection <= 0 || selection > doc.getRecords().size()) {
		selection = 1;
		cout<<"Value out of range.\n";
		cout<<"Press any key to continue.\n";
		awaitKey();
	}
}

void Table::up() {
	if(selection > 1) {
		selection--;
	}
}

void Table::down() {
	++selection;
	if(selection > doc.getRecords().size()) {
		--selection;
	}
}

void Table::removeRecord() {
	doc.removeRecord(selection);
	update();
}

void Table::sortByName() const {
	doc.sortByName();
}

void Table::sortByPoints() const {
	doc.sortByPoints();
}

void Table::addRecord() const {
	cout<<"Enter name followed by number of points: ";
	string n; 
	int p;
	cin>>n>>p;
	doc.addRecord(n,p);
}

void Table::loadFromFile() const {
	doc.clear();
	string text;
	int num;
	cout<<"Enter the name of file: ";
	cin>>text;
	ifstream fs;
	fs.open(text.c_str());
	if(!fs) {
		cout<<"Failed to read file \""<<text<<"\".\n";
	}
	else {
		while(fs>>text) {
			fs>>num;
			if(fs.fail()) {
				cout<<"Warning. Incomplete file.\n";
				break;
			}
			else {
				doc.addRecord(text, num);
			}
		} 
	}
	fs.close();
}

void Table::saveToFile() const {
	string text;
	cout<<"Enter the name of file: ";
	cin>>text;
	ofstream fs;
	fs.open(text.c_str());
	if(fs.is_open()) {
		const list<Record>& rec = doc.getRecords();
		for(list<Record>::const_iterator it=rec.begin();it!=rec.end();++it) {
			fs<<it->name<<" "<<it->points<<"\n";
		}
	}
	fs.close();
}

void Table::processActions() {
	Board::processKey();
	processKey();
}

void Table::processKey() {
	if(key == "\x1b[A") {
		up();
	}
	else if(key == "\x1b[B") {
		down();
	}
	else if(key == "n") {
		sortByName();
	}
	else if(key == "p") {
		sortByPoints();
	}
	else if(key == "s") {
		select();
	}
	else if(key == "S") {
		saveToFile();
	}
	else if(key == "L") {
		loadFromFile();
	}
	else if(key == "a") {
		addRecord();
	}
	else if(key == "r") {
		removeRecord();
	}
}