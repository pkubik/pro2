/* 
 * Pawel Kubik
 * Klasa glowna programu
 */

#include "Program.h"
#include "readkey.h"

Program::Program() : doc(), tab(doc, key), chart(doc, key) {
	doc.addView(tab);
	doc.addView(chart);
	quitFlag = false;
}

void Program::passControl() {
	doc.draw();
	char ch[5];
	readKey(ch,5);
	key = ch;
	if(!key.compare("\x1b")) {
		quitFlag = true;
	}
	doc.passControl();
}
