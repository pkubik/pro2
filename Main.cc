/* 
 * Pawel Kubik
 * Main
 */

#include "readkey.h"
#include "Program.h"

using namespace std;

int main() {
	initReadKey();
	Program scoreboard;
	while(!scoreboard.getQuitFlag()) {
		scoreboard.passControl();
	}
    return 0;
}

