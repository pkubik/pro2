/* 
 * Pawel Kubik
 * Implementacja widoku tabeli
 */

#include "Board.h"

#ifndef TABLE_H
#define	TABLE_H

class Table : public Board {
private:
	unsigned int selection;
public:
	Table(Score& document, string& k);
	virtual void draw() const;
	virtual void update();
	virtual void processActions();
	virtual void processKey();
	void select();
	void up();
	void down();
	void removeRecord();
	void sortByName() const;
	void sortByPoints() const;
	void addRecord() const;
	void loadFromFile() const;
	void saveToFile() const;
};

#endif	/* TABLE_H */

